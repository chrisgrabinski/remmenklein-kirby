Title: Automobilhandel

----

Text: 

Unser Beratungsspektrum für den Automobilvertrieb umfasst insbesondere die Bereiche:

- Restrukturierung von Handelsstrukturen
- Gesellschaftsrecht und Unternehmensfinanzierung sowie
- Kartellrecht

Die Gruppenfreistellungsverordnung (GVO) ist und bleibt zentrale Instanz für den Automobilvertrieb. Jedoch sind derzeit weniger die Änderungen der rechtlichen Rahmenbedingungen als vielmehr die neuerliche Marktentwicklung, kombiniert mit politischen Rahmenbedingungen und neuen Trends im Automobilvertrieb, Treiber für Veränderungen.

Wir beraten den Automobilhandel daher bei der Restrukturierung von Vertriebsstrukturen, die der wirtschaftliche Druck notwendig macht, insbesondere im Zusammenhang mit:

- Beschaffungsverträgen 
- Vertragsrecht 
- Kooperationsvereinbarungen 
- Konsolidierung und Restrukturierung 
- Expansion (Mergers & Acquisitions)
- Unternehmensverkäufen
- Outsourcing
- Sonstigen Allianzen (finanzgetrieben oder strategisch)
- Verschmelzungen, Ausgliederungen, Auf- und Abspaltungen (Umwandlungsgesetz) 
- Restrukturierung außerhalb des Umwandlungsgesetzes 
- Gesellschaftsrecht 
- Private Equity 
- Prozessführung 
- Investmentfragen 
- Insolvenzen
- Arbeitsrecht

Das Gesellschafts- und Steuerrecht unterwirft die Fremd- und Eigenkapitalfinanzierung außerdem strengen Regeln. Wir beraten unsere Mandanten im Automobilhandel bei der Vorbereitung, der Verhandlung der notwendigen Verträge sowie bei der Durchführung von:  

- Expansions-Finanzierung (Eigenkapitalmaßnahmen, Fremdkapitalfinanzierung, Kapitalerhöhungen), 
- Konzern-Finanzierung (steuerlich motivierte Konzern-Refinanzierung),
- Mezzanine Finanzierung (Genussrechte, Optionen, atypische/typische stille Beteiligungen und  Unterbeteiligungen)

Dabei stimmen wir die Strukturen der Finanzierungsmaßnahmen im Einzelfall eng mit den Steuerberatern und Wirtschaftsprüfern unserer Mandanten ab.

Wir beraten unsere Mandanten im Automobilhandel ferner bei:

- Unternehmenskäufen und Unternehmensverkäufen 
- Neugründungen von Gemeinschaftsunternehmen 
- Einzelabreden zwischen Unternehmen (Kooperations-, Übernahme- und sonstige Verträge) sowie
- Preisgestaltung und Gestaltung der Lieferbeziehungen zu bestimmten Geschäftspartnern im Hinblick auf Ausschließlichkeit und Wettbewerbsverbote