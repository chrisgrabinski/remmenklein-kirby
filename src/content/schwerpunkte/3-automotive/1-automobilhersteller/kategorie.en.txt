Title: Manufacturers

----

Text: 

We advise automobile manufacturers, intra-group importers and wholesalers in particular in the areas of

- sourcing
- sales law
- contract law (R&D-cooperations)
- other cooperation agreements
- restructuring of wholesale and retail structures (M&A)
- brand protection

----

Url-key: manufacturers