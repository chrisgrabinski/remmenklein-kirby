Title: Commercial and Sales Law

----

Text: 

##Our highly focused industry approach is based on our high degree of specialisation and many years of experience in a number of selected fields of law.

Our corporate-team consists of our highly specialised corporate lawyers, including our network of preferred corporation partners (national and international) who each are among the leading independent law firms in each of their area of expertise.

Our corporate-team offers business-oriented legal advise and handles court actions (Prozessführung) in commercial, sales and distribution law. Our services expressly aim at creating added value for our clients.

We provide legal advice and assistance on a wide range of commercial agreements, from strategic planning of transactions and negotiation to deal implementation and contract management. Our corporate-team advises you inter alia in connection with:

- framework agreements
- production and supply agreements
- sourcing agreements
- general terms and conditions of purchase
- general terms and conditions of sale
- license agreements
- other contracts (e.g. R&D-cooperations)
- outsourcing
- sales and distribution agreements (sales law)
- agent agreements and termination
- dealership agreements and termination
- franchise agreements and termination
- restructuring of wholesale and retail structures

##Clients

- corporate founders
- companies in all industries
- domestic and international investors

----

Phone: +49 (0) 2 11 / 87 63 80 - 33

----

Url-key: commercial-and-sales-law

----

Show-siblings: 1

----

Subtext: 

##Do you have any further questions?

Please do not hesitate to contact us.

----

Email: 

----

Sidebar: beratungsansatz