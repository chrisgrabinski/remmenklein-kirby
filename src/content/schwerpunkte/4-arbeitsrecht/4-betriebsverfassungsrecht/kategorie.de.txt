Title: Betriebsverfassungsrecht

----

Text: 

Die Auseinandersetzungen zwischen Arbeitgeber und Betriebsräten werden vor dem Hintergrund des sich ständig verschärfenden Wettbewerbsdrucks immer intensiver geführt. Es ist daher von besonderer Bedeutung, die betriebsverfassungsrechtlichen Rechte und Pflichten genau zu kennen, um seine Interessen sachgerecht vertreten zu können und vernünftige Ergebnisse auch vor dem Hintergrund belasteter Beziehungen erzielen zu können. Wir helfen unseren Mandanten dabei, sich zu positionieren, Fehler zu vermeiden und optimale Verhandlungsergebnisse zu erzielen. 

Unsere Tätigkeitsschwerpunkte hierbei sind:

Einleitung und Begleitung von Einigungsstellenverfahren
Personelle Mitbestimmung (Versetzungen, Einstellungen pp.)
Prüfung und Gestaltung von Betriebsvereinbarungen
Tätigkeit als Sachverständiger
Beschlussverfahren vor dem Arbeitsgericht
Erstellung und Verhandlung von Interessenausgleich und Sozialplänen