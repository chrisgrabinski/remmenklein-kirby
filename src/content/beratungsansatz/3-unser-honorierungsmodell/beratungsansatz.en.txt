Title: Our Fee Model

----

Text: 

The increasing dissatisfaction of the clients with the inflated hourly rates of big law firms inter alia led to establish our law firm. Today, we are a small but outstanding group of highly specialised lawyers with clear and cost-wise lean structures. We are capable of offering our services cost-efficiently and in every respect flexible. 

We either charge our services on the basis of hourly rates or on the basis of an all-inclusive budget determined in advance.
 
We are so capable of offering an adequate fee model to our clients in each individual case.

Experience shows that our offer discharges the budgets of our clients. With respect to the remuneration we try to establish a long lasting relationship to our clients as well.