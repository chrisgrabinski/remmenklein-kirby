Title: Our Specialisation Approach

----

Text: 

We are specialised in advising the automotive industry. This requires knowledge of the legal structures of the automotive industry as well as the sound know-how of the economic forces within the automotive industry. 

Our highly focused industry approach is based on our high degree of specialisation and many years of experience in a number of selected fields of law.

Our corporate-team consists of our highly specialised corporate lawyers, including our network of preferred cooporation partners (national and international) who each are among the leading independent law firms in each of their area of expertise.

Our corporate-team offers business-oriented legal advise and handles court actions (Prozessführung) in the areas:

- corporate
- commercial, sales and distribution
- brand protection.