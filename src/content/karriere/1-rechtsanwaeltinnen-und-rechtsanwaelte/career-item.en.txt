Title: Lawyers

----

Text: 

If you have completed your studies with above-average results and possess good knowledge of the English language, and if your education and legal career evidence your experience and interest in the fields of commercial and company law, we would love to hear from you!

Through continual integration in a team, as a lawyer at our firm you have the chance to comprehensively advise and meet the very high demands of our clients. In our teamwork, we place great emphasis on an intensive education of our lawyers, but also on the importance of our lawyers’ development in advising our clients independently. We require our lawyers to publish and make presentations in their specialised field.

In addition to their legal qualities, a fair and friendly manner, as well as the ability to work in a team, are the qualities we seek in our workers. In our opinion, having fun in your job, an excellent work atmosphere and the continual exchange of ideas are indispensable both to the effective performance of individuals and to the success of the law firm.

We offer you a working environment equipped with state-of-the-art technology and provide you with colleagues that are easily approachable. 

We always welcome law students wishing to gain an insight into our working environment and “life at the firm” during their legal studies.

We look forward to your application. Please send your application to:

Remmen Klein Partnerschaft von Rechtsanwälten mbB
z. Hd. Herrn Dr. Peter Remmen
Jan-Wellem-Platz 3
40212 Düsseldorf
Germany

<bewerbung@remmen-klein.de>

----

Url-key: lawyers

----

Address: 

Remmen Klein Partnerschaft von Rechtsanwälten mbB
z. Hd. Herrn Dr. Peter Remmen
Jan-Wellem-Platz 3
40212 Düsseldorf

----

Email: bewerbung@remmen-klein.de