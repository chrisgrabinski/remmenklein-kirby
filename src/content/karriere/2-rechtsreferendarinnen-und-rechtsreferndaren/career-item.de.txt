Title: Rechtsreferendarinnen und Rechtsreferndare

----

Text: 

Idealerweise haben Sie das erste Staats­examen mit über­durch­schnittlichem Erfolg abge­schlossen. Sie erhalten in den verschiedenen Aus­bildungs­abschnitten des Referendariats die Möglich­keit, in unserer Kanzlei Ihren Erfahrungen und dem Stand Ihrer Ausbildung gemäß mitzuarbeiten.

Wir freuen uns auf Ihre Bewerbung.

----

Address: 

Remmen Klein Partnerschaft von Rechtsanwälten mbB
z. Hd. Herrn Dr. Peter Remmen
Jan-Wellem-Platz 3
40212 Düsseldorf

----

Email: bewerbung@remmen-klein.de