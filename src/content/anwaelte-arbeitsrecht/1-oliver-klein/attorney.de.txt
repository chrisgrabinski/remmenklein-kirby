Title: Oliver Klein

----

Field: Rechtsanwalt und Fachanwalt für Arbeitsrecht

----

Text: 

Rechtsanwalt und Fachanwalt für Arbeitsrecht Oliver Klein berät im Arbeitsrecht schwerpunktmäßig Führungskräfte, leitende Angestellte, Geschäftsführer und Vorstände mittelständischer Unternehmen. Insbesondere die strategische Beratung sowie die Begleitung im Tagesgeschäft sowie Trennungsverhandlungen und Vergütungsstreitigkeiten (Bonus, Aktien pp.) gehören hierbei zu seinen Tätigkeitsfeldern. Er ist darüber hinaus bekannt für seine beratende Tätigkeit für Gesamtbetriebsräte, insbesondere im Bereich IT, Telekommunikation und Fluggesellschaften.

Im JUVE Handbuch für Wirtschaftskanzleien 2010/2011 wird Rechtsanwalt Klein von Wettbewerbern als „sehr kompetent“ beschrieben. Die Einheit um Oliver Klein wird als umtriebig, konstruktiv und ehrlich wahrgenommen (JUVE 2012/2013).

Oliver Klein ist Lehrbeauftragter der Verwaltungs- und Wirtschaftsakademie in Essen, Referent für mehrere Bildungseinrichtungen und Autor arbeitsrechtlicher Publikationen, u.a. für Neue Zeitschrift für Arbeitsrecht (NZA), Neues Arbeitsrecht für Vorgesetzte, anwalt24.de etc.

----

Contactphone: +49 (0) 2 11 / 87 63 80 - 44

----

Contactmail: 