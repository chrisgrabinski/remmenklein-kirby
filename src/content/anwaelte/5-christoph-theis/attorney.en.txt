Title: Christoph Theis

----

Text: 

Herr Rechtsanwalt Christoph Theis  berät schwerpunktmäßig Führungskräfte und Leitende Angestellte.  Er besitzt als Teil des Teams um den Partner Oliver Klein mehrjährige Erfahrung insbesondere in der prozessualen Auseinandersetzung wegen betriebsbedingter und verhaltensbedingter Kündigungen sowie der Beratung von Führungskräften. Ein weiterer Schwerpunkt ist die Überprüfung und Gestaltung von Arbeitsverträgen. 

Herr Theis hat eine ausgeprägte Verhandlungskompetenz. Er ist dafür bekannt, die rechtlichen Probleme eines Falles schnell zu durchdringen und die Interessen seiner Mandanten entschlossen zu vertreten. 

Herr Theis ist im Bereich des Arbeitsrechts als Referent tätig.

----

Field: Lawyer

----

Contactphone: +49 (0) 2 11 / 87 63 80 - 44

----

Contactmail: 