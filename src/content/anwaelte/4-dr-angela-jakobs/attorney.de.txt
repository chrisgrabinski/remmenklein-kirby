Title: Dr. Angela Jakobs

----

Text: 

Dr. Angela Jakobs berät Unternehmen und Führungskräfte in allen zivil- und gesell­schafts­rechtlichen Fragestellungen, insbesondere bei der Gestaltung und Prüfung von Vertragskonzepten und der Begutachtung der vertraglichen Verteilung von wirtschaftlichen Risiken und Haftungsrisiken. Sie betreut Unternehmen bei der Erledigung der regelmäßig anfallenden gesellschafts­rechtlichen Maßnahmen (corporate house keeping).

Dr. Angela Jakobs berät Unternehmen des Mittelstandes sowie Konzernunternehmen u.a. in den Beratungsfeldern: 

- Mergers & Acquisitions (Unternehmenskäufe und -verkäufe)
- Handels- und Gesellschaftsrecht
- Joint-Venture-Agreements
- Strategische Kooperationsverträge
- Einkaufsverträge und Vertriebsverträge
- Restrukturierung

Sie promovierte im Bereich des Aktienrechts zum Thema „Die Rechte des Minderheitsaktionärs beim aktienrechtlichen Squeeze-out“.

----

Field: Rechtsanwältin

----

Contactphone: +49 (0) 2 11 / 87 63 80 - 33

----

Contactmail: 