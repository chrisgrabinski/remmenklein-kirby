$('.header-area-button-menu').on('click', function() {
  $('body').toggleClass('navigation-is-active');
});

$('.news-widget-link').each(function()
{
  var slug = $(this).attr('id'),
      href = $(this).attr('href');

  $(this).attr('href', href.replace(slug, '') + '#' + slug);
});

$('.news-widget-link, .arbeitsrecht .widget-footer a').on('click', function(event)
{
  event.preventDefault();
  var slug = '#' + $(this).attr('id');
  var t    = $(this);

  $('.news-layer .accordion-title-is-open').removeClass('accordion-title-is-open').next().hide();

  $('body').addClass('news-layer-is-active');
  //$('.content-area > .container').delay(400).slideUp(500);

  if ( t.hasClass('news-widget-link') ) {
    $('.news-layer ' +  slug).addClass('accordion-title-is-open').next().slideDown();
  }

  $('.close-news').on('click', function() {
    //$('.content-area > .container').slideDown(800);
    $('body').removeClass('news-layer-is-active');
  });
});


$('.home').addClass('navigation-is-active');

var hash = window.location.hash;
if ( hash ) {
  $('.news .accordion-title' + hash).addClass('accordion-title-is-open').next().show();
}

$(".animsition").animsition({

  inClass               :   'fade-in',
  outClass              :   'fade-out',
  inDuration            :    1000,
  outDuration           :    800,
  linkElement           :   '.animsition-link',
  // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
  loading               :    true,
  loadingParentElement  :   'body', //animsition wrapper element
  loadingClass          :   'animsition-loading',
  unSupportCss          : [ 'animation-duration',
                            '-webkit-animation-duration',
                            '-o-animation-duration'
                          ],
  //"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
  //The default setting is to disable the "animsition" in a browser that does not support "animation-duration".

  overlay               :   false,

  overlayClass          :   'animsition-overlay-slide',
  overlayParentElement  :   'body'
});


function accordionTitle() {
  $('.accordion-title').on('click', function(event) {
    event.stopPropagation();
    $(this).toggleClass('accordion-title-is-open').next().slideToggle();
  });
} accordionTitle();

function categoryTitle() {
  $('.category__header').on('click', function(event) {
    event.stopPropagation();
    $(this).closest('.category').toggleClass('category--open').find('.category__body').slideToggle();
  });
} categoryTitle();

$('.is-ie8 .navigation-primary .navigation-item:first-child, .is-ie8 .navigation-primary .navigation-item:last-child').css('margin', '0 33%');
