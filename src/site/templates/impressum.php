<?php snippet('header') ?>

  <main class="content-area content-is-primary" role="content">
    <div class="container">
      <article class="article">
        <header class="article-header">
          <h1 class="article-title"><span><?php echo $page->title()->html() ?></span></h1>
        </header>
        <div class="article-body">
          <div class="usercontent">
            <h2><?php echo $site->author()->html() ?></h2>
            <p>
              <span><?php echo $site->street()->html() ?></span><br>
              <span><?php echo $site->zip()->html() ?></span> <span><?php echo $site->locality()->html() ?></span><br>
              <span><?php echo $site->country()->html() ?></span>
            </p>
          </div>
          <ul class="contact-list">
            <li class="contact-item contact-item-email"><a href="mailto:<?php echo $site->email()->html(); ?>?subject=<?php echo urlencode( '[remmen-klein.de] Kontaktanfrage über '. $page->title()->html() ); ?>"><span><?php echo $site->email()->html(); ?></span></a></li>
            <li class="contact-item contact-item-phone"><a href="tel:<?php echo preg_replace('/[^0-9\+]/', '', $site->phone()->html() ); ?>"><span><?php echo $site->phone()->html(); ?></span></a></li>
            <li class="contact-item contact-item-fax"><a href="tel:<?php echo preg_replace('/[^0-9\+]/', '', $site->phone()->html() ); ?>"><span><?php echo $site->fax()->html(); ?></span></a></li>
          </ul>
          <div class="usercontent">
            <hr>
            <?php echo $page->text()->kirbytext() ?>
          </div>
        </div>
      </article>
    </div>
  </main>

<?php snippet('footer') ?>
