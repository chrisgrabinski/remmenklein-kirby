<?php snippet('header') ?>

  <main class="content-area content-is-white" role="content">
    <div class="container">
      <article class="article">
        <header class="article-header">
          <h1 class="article-title"><span><?php echo $page->title()->html() ?></span></h1>
        </header>
        <div class="article-body">
          <?php if ( $page->children()->visible()->count() >= 1 ) : ?>
          <dl class="accordion">
            <?php foreach($page->children()->visible() as $news): ?>
            <dt id="<?php echo $news->slug() ?>" class="accordion-title">
              <span><?php echo $news->title()->html() ?></span>
              <time class="accordion-time" datetime="<?php echo $news->date('c') ?>"><?php echo $news->date('d.m.Y') ?></time>
            </dt>
            <dd class="accordion-content">
              <div class="usercontent">
                <?php echo $news->text()->kirbytext() ?>
              </div>
            </dd>
            <?php endforeach ?>
          </dl>
          <?php endif; ?>
        </div>
      </article>
    </div>
  </main>

<?php snippet('footer') ?>
