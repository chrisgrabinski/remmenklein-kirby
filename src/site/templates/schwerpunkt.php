<?php snippet('header') ?>

  <main class="content-area" role="content">
    <div class="container">
      <article class="article <?php if ( $page->sidebar() != "none" ) : ?>article-has-aside<?php endif ?>">
        <header class="article-header">
          <h1 class="article-title"><span><?php echo $page->title()->html() ?></span></h1>
        </header>
        <div class="article-body">
          <?php if( !$page->text()->empty() ): ?>
          <div class="usercontent">
            <?php echo $page->text()->kirbytext() ?>
          </div>
          <?php endif; ?>
          <?php if ( $page->children()->children()->visible()->count() >= 1 ) : ?>
            <?php foreach($page->children()->visible() as $category): ?>
              <div class="category">
                <div class="category__header">
                  <h2 class="category__title"><?php echo $category->title()->html() ?></h2>
                </div>
                <div class="category__body">
                  <?php if ( $category->text()->kirbytext() ) : ?>
                    <div class="usercontent">
                      <?php echo $category->text()->kirbytext() ?>
                    </div>
                  <?php endif; ?>
                  <dl class="accordion">
                    <?php foreach($category->children()->visible() as $subtopic): ?>
                    <dt class="accordion-title"><span><?php echo $subtopic->title()->html() ?></span></dt>
                    <dd class="accordion-content">
                      <div class="usercontent">
                        <!--<h2><?php echo $subtopic->title()->html() ?></h2>-->
                        <?php echo $subtopic->text()->kirbytext() ?>
                      </div>
                      <?php if ( !$page->phone()->empty() || !$page->email()->empty() ) : ?>
                      <ul class="contact-list">
                        <?php if ( !$page->phone()->empty() ) : ?>
                        <li class="contact-item contact-item-phone"><a href="tel:<?php echo preg_replace('/[^0-9\+]/', '', $page->phone()->html() ); ?>"><span><?php echo $page->phone()->html(); ?></span></a></li>
                        <?php endif; ?>
                        <?php if ( !$page->email()->empty() ) : ?>
                        <li class="contact-item contact-item-email"><a href="mailto:<?php echo $page->email()->html(); ?>"><span><?php echo $page->email()->html(); ?></span></a></li>
                        <?php endif; ?>
                      </ul>
                      <?php endif; ?>
                    </dd>
                    <?php endforeach; ?>
                  </dl>
                </div>
              </div>
            <?php endforeach; ?>
          <?php elseif ( $page->children()->visible()->count() >= 1 ) : ?>
            <dl class="accordion">
              <?php foreach($page->children()->visible() as $subtopic): ?>
              <dt class="accordion-title"><span><?php echo $subtopic->title()->html() ?></span></dt>
              <dd class="accordion-content">
                <div class="usercontent">
                  <?php echo $subtopic->text()->kirbytext() ?>
                </div>
                <?php if ( !$page->phone()->empty() || !$page->email()->empty() ) : ?>
                <ul class="contact-list">
                  <?php if ( !$page->phone()->empty() ) : ?>
                  <li class="contact-item contact-item-phone"><a href="tel:<?php echo preg_replace('/[^0-9\+]/', '', $page->phone()->html() ); ?>"><span><?php echo $page->phone()->html(); ?></span></a></li>
                  <?php endif; ?>
                  <?php if ( !$page->email()->empty() ) : ?>
                  <li class="contact-item contact-item-email"><a href="mailto:<?php echo $page->email()->html(); ?>"><span><?php echo $page->email()->html(); ?></span></a></li>
                  <?php endif; ?>
                </ul>
                <?php endif; ?>
              </dd>
              <?php endforeach; ?>
            </dl>
          <?php endif; ?>
          <?php if( !$page->subtext()->empty() ): ?>
            <div class="usercontent">
              <?php echo $page->subtext()->kirbytext() ?>
            </div>
          <?php endif; ?>
          <?php if ( !$page->children()->visible() || !$page->subtext()->empty() ) : ?>
          <?php if ( !$page->phone()->empty() || !$page->email()->empty() ) : ?>
          <ul class="contact-list">
            <?php if ( !$page->phone()->empty() ) : ?>
            <li class="contact-item contact-item-phone"><a href="tel:<?php echo preg_replace('/[^0-9\+]/', '', $page->phone()->html() ); ?>"><span><?php echo $page->phone()->html(); ?></span></a></li>
            <?php endif; ?>
            <?php if ( !$page->email()->empty() ) : ?>
            <li class="contact-item contact-item-email"><a href="mailto:<?php echo $page->email()->html(); ?>"><span><?php echo $page->email()->html(); ?></span></a></li>
            <?php endif; ?>
          </ul>
          <?php endif; ?>
          <?php endif; ?>
          <?php if ( $page->show_siblings()->bool() ) : ?>
          <ul class="fields-list">
            <?php foreach($pages->find('schwerpunkte')->children()->not($page)->not('arbeitsrecht')->not('automotive')->sortBy('title', 'asc') as $p): ?>
            <li class="field-item">
              <a class="field-link" href="<?php echo $p->url(); ?>">
                <span class="field-title"><?php echo $p->title()->html(); ?></span>
              </a>
            </li>
            <?php endforeach; ?>
            <?php if ( $page->id() != 'schwerpunkte/automotive' ) : ?>
            <li class="field-item">
              <a class="field-link" href="<?php echo $pages->find('schwerpunkte')->children()->find('automotive')->url(); ?>">
                <span class="field-title"><?php echo $pages->find('schwerpunkte')->children()->find('automotive')->title()->html(); ?></span>
              </a>
            </li>
            <?php endif; ?>
          </ul>
          <?php endif; ?>
        </div>
        <?php if ( $page->sidebar() != "none" ) : ?>
        <aside class="article-aside <?php if ( $page->sidebar() == 'beratungsansatz' ) : ?>article-aside--beratungsansatz<?php endif; ?>" role="complementary">
          <div class="widget">
            <div class="widget-header">
              <h2 class="widget-title">
                <?php if ( $page->sidebar() == 'news' ) {
                  echo $pages->find('news')->title()->html();
                } else if ( $page->sidebar() == 'beratungsansatz' ) {
                  echo $pages->find('beratungsansatz')->title()->html();
                } ?>
            </div>
            <div class="widget-body">
              <ul class="news-widget">
                <?php if ( $page->sidebar() == 'news' ) {
                  foreach($pages->find('news')->children()->visible()->limit(3) as $news): ?>
                  <li class="news-widget-item">
                    <a id="<?php echo $news->slug() ?>" class="news-widget-link" href="<?php echo $news->url() ?>">
                      <time class="news-widget-date" datetime="<?php echo $news->date('c') ?>"><?php echo $news->date('d.m.Y') ?></time>
                      <h3 class="news-widget-title"><?php echo $news->title()->html() ?></h3>
                    </a>
                  </li>
                  <?php endforeach;
                } else if ( $page->sidebar() == 'beratungsansatz' ) {
                  foreach($pages->find('beratungsansatz')->children()->visible() as $news): ?>
                  <li class="news-widget-item">
                    <a id="<?php echo $news->slug() ?>" class="news-widget-link" href="<?php echo $news->url() ?>">
                      <h3 class="news-widget-title"><?php echo $news->title()->html() ?></h3>
                    </a>
                  </li>
                  <?php endforeach;
                } ?>
              </ul>
            </div>
            <div class="widget-footer">
              <a href="<?php if ( $page->sidebar() == 'news' ) {
                echo $pages->find('news')->url();
              } else if ( $page->sidebar() == 'beratungsansatz' ) {
                echo $pages->find('beratungsansatz')->url();
              } ?>"><?php echo l::get('show-all') ?></a>
            </div>
          </div>
        </aside>
        <?php endif; ?>
      </article>
    </div>
  </main>
  <?php if ( $page->sidebar() != "none" ) : ?>
  <div class="news-layer">
    <div class="container">
      <article class="article">
        <header class="article-header">
          <h1 class="article-title"><span><?php if ( $page->sidebar() == 'news' ) {
            echo $pages->find('news')->title()->html();
          } else if ( $page->sidebar() == 'beratungsansatz' ) {
            echo $pages->find('beratungsansatz')->title()->html();
          } ?></span></h1>
          <button class="close-news"><span>Close</span></button>
        </header>
        <div class="article-body">
          <?php if( $page->sidebar() == "news"): ?>
            <?php if( $pages->find('news')->headline() != '' ): ?>
            <div class="usercontent">
              <h2><?php echo $pages->find('news')->headline()->html() ?></h2>
            </div>
            <?php endif; ?>
            <?php if ( $pages->find('news')->children()->visible()->count() >= 1 ) : ?>
            <dl class="accordion">
              <?php foreach($pages->find('news')->children()->visible() as $news): ?>
              <dt id="<?php echo $news->slug() ?>" class="accordion-title">
                <span><?php echo $news->title()->html() ?></span>
                <time class="accordion-time" datetime="<?php echo $news->date('c') ?>"><?php echo $news->date('d.m.Y') ?></time>
              </dt>
              <dd class="accordion-content">
                <div class="usercontent">
                  <?php echo $news->text()->kirbytext() ?>
                </div>
              </dd>
              <?php endforeach ?>
            </dl>
            <?php endif; ?>
          <?php endif; ?>

          <?php if( $page->sidebar() == 'beratungsansatz'): ?>
            <?php if( $pages->find('beratungsansatz')->headline() != '' ): ?>
            <div class="usercontent">
              <h2><?php echo $pages->find('beratungsansatz')->headline()->html() ?></h2>
            </div>
            <?php endif; ?>
            <?php if ( $pages->find('beratungsansatz')->children()->visible()->count() >= 1 ) : ?>
            <dl class="accordion">
              <?php foreach($pages->find('beratungsansatz')->children()->visible() as $support): ?>
              <dt id="<?php echo $support->slug() ?>" class="accordion-title">
                <span><?php echo $support->title()->html() ?></span>
                <time class="accordion-time" datetime="<?php echo $support->date('c') ?>"><?php echo $support->date('d.m.Y') ?></time>
              </dt>
              <dd class="accordion-content">
                <div class="usercontent">
                  <?php echo $support->text()->kirbytext() ?>
                </div>
              </dd>
              <?php endforeach ?>
            </dl>
            <?php endif; ?>
          <?php endif; ?>
        </div>
      </article>
    </div>
  </div>
  <?php endif; ?>

<?php snippet('footer') ?>
