<?php snippet('header') ?>

  <main class="content-area" role="content">
    <div class="container">
      <article class="article">
        <header class="article-header">
          <h1 class="article-title"><span><?php echo $page->title()->html() ?></span></h1>
        </header>
        <div class="article-body">
          <?php foreach(page()->children()->visible() as $attorney): ?>
          <div class="attorney">
            <h2 class="attorney-name"><?php echo $attorney->title()->html() ?></h2>
            <p class="attorney-specialty"><?php echo $attorney->field()->html() ?></p>
            <?php foreach($attorney->images() as $img): ?>
              <img class="attorney-image" src="<?php echo $img->url() ?>" alt="<?php echo $img->title() ?>">
            <?php endforeach ?>
          </div>
          <dl class="accordion">
            <dt class="accordion-title"><span><?php echo $page->label()->html() ?></span></dt>
            <dd class="accordion-content">
              <div class="usercontent">
                <?php echo $attorney->text()->kirbytext() ?>
              </div>
              <?php if ( !$attorney->contactphone()->empty() || !$attorney->contactmail()->empty() ) : ?>
              <ul class="contact-list">
                <?php if ( !$attorney->contactphone()->empty() ) : ?>
                <li class="contact-item contact-item-phone"><a href="tel:<?php echo preg_replace('/[^0-9\+]/', '', $attorney->contactphone()->html() ); ?>"><span><?php echo $attorney->contactphone()->html(); ?></span></a></li>
                <?php endif; ?>
                <?php if ( !$attorney->contactmail()->empty() ) : ?>
                <li class="contact-item contact-item-email"><a href="mailto:<?php echo $attorney->contactmail()->html(); ?>"><span><?php echo $attorney->contactmail()->html(); ?></span></a></li>
                <?php endif; ?>
              </ul>
              <?php endif; ?>
            </dd>
          </dl>
          <?php endforeach; ?>
        </div>
      </article>
    </div>
  </main>

<?php snippet('footer') ?>
