<?php
  $company = $site->author()->escape();
  $street = $site->street()->escape();
  $zip = $site->zip()->escape();
  $city = $site->locality()->escape();
  $region = $site->region()->escape();
  $country = $site->country()->escape();

  $email = $site->email()->escape();
  $phone = preg_replace('/[^0-9\+]/', '', $site->phone()->escape());
  $fax = preg_replace('/[^0-9\+]/', '', $site->fax()->escape());
  $url = $site->url();

  header('Content-Disposition: attachment; filename=remmenklein.vcf');
  header('Content-Type: text/vcard; charset=iso-8859-1');

  print "BEGIN:VCARD\n";
  print "VERSION:3.0\n";
  print "ADR;INTL;PARCEL;WORK:;;". $street .";". $city .";". $region .";". $zip .";". $country ."\n";
  print "EMAIL;INTERNET:". $email ."\n";
  print "ORG:". $company ."\n";
  print "TEL;WORK:". $phone ."\n";
  //print "TEL;FAX;WORK:". $fax ."\n";
  print "URL;WORK:". $url ."\n";
  print "END:VCARD";

?>
