<?php snippet('header') ?>

  <main class="content-area <?php if ( $page->id() == 'news-item' ) : ?>content-is-white<?php endif ?>" role="content">
    <div class="container">
      <article class="article">
        <header class="article-header">
          <h1 class="article-title"><span><?php echo $page->title()->html() ?></span></h1>
        </header>
        <div class="article-body">
          <div class="usercontent">
            <?php echo $page->text()->kirbytext() ?>
          </div>
          <?php if ( $page->children()->visible()->count() >= 1 ) : ?>
          <dl class="accordion">
          <?php foreach($page->children()->visible() as $subfield): ?>
            <dt class="accordion-title"><span><?php echo $subfield->title()->html() ?></span></dt>
            <dd class="accordion-content">
              <div class="usercontent">
                <?php echo $subfield->text()->kirbytext() ?>
              </div>
              <?php if ( !$subfield->address()->empty() ) : ?>
              <div class="usercontent">
                <?php echo $subfield->address()->kirbytext() ?>
              </div>
              <?php endif; ?>
              <?php if ( !$subfield->phone()->empty() || !$subfield->email()->empty() ) : ?>
              <ul class="contact-list">
                <?php if ( !$subfield->phone()->empty() ) : ?>
                <li class="contact-item contact-item-phone"><a href="tel:<?php echo preg_replace('/[^0-9\+]/', '', $subfield->phone()->html() ); ?>"><span><?php echo $subfield->phone()->html(); ?></span></a></li>
                <?php endif; ?>
                <?php if ( !$subfield->email()->empty() ) : ?>
                <li class="contact-item contact-item-email"><a href="mailto:<?php echo $subfield->email()->html(); ?>"><span><?php echo $subfield->email()->html(); ?></span></a></li>
                <?php endif; ?>
              </ul>
              <?php endif; ?>
            </dd>
          <?php endforeach ?>
          </dl>
          <?php endif; ?>
        </div>
      </article>
    </div>
  </main>

<?php snippet('footer') ?>
