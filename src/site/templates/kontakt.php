<?php snippet('header') ?>

  <main class="content-area" role="content">
    <div class="container">
      <article class="article">
        <header class="article-header">
          <h1 class="article-title"><span><?php echo $page->title()->html() ?></span></h1>
          <a class="article-header-link" href="<?php echo $page->find('save-contact')->url() ?>" title="<?php echo l::get('save-contact') ?>"><span><?php echo l::get('save-contact') ?></span></a>
        </header>
        <div class="article-body">
          <div class="usercontent">
            <h2><?php echo $site->author()->html() ?></h2>
            <p>
              <span><?php echo $site->street()->html() ?></span><br>
              <span><?php echo $site->zip()->html() ?></span> <span><?php echo $site->locality()->html() ?></span><br>
              <span><?php echo $site->country()->html() ?></span>
            </p>
          </div>
          <ul class="contact-list">
            <li class="contact-item contact-item-email"><a href="mailto:<?php echo $site->email()->html(); ?>?subject=<?php echo urlencode( '[remmen-klein.de] Kontaktanfrage über '. $page->title()->html() ); ?>"><span><?php echo $site->email()->html(); ?></span></a></li>
            <li class="contact-item contact-item-phone"><a href="tel:<?php echo preg_replace('/[^0-9\+]/', '', $site->phone()->html() ); ?>"><span><?php echo $site->phone()->html(); ?></span></a></li>
            <li class="contact-item contact-item-fax"><a href="tel:<?php echo preg_replace('/[^0-9\+]/', '', $site->phone()->html() ); ?>"><span><?php echo $site->fax()->html(); ?></span></a></li>
          </ul>
          <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDt_9YoVZzX24uwdqGR0ExO2CQ6Z2qMKbI">
          </script>
          <script type="text/javascript">
            function initialize() {

              var myLatlng = new google.maps.LatLng(<?php echo $site->latlng()->html() ?>);

              var mapOptions = {
                center: myLatlng,
                zoom: 17,
                panControl: false,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: true,
                streetViewControl: false,
                overviewMapControl: false,
                scrollwheel: false
              };
              var map = new google.maps.Map(document.getElementById('map-canvas'),
                  mapOptions);

              var url = '<?php echo url('assets/images/map-marker.png') ?>';
              var size = new google.maps.Size(64, 64);
              if(window.devicePixelRatio >= 1.5){
                url = '<?php echo url('assets/images/map-marker@2x.png') ?>';
                size = new google.maps.Size(128, 128);
              }

              var image = {
                url: url,
                size: size,
                scaledSize: new google.maps.Size(64, 64),
                origin: new google.maps.Point(0,0),
                anchor: new google.maps.Point(32, 54)
              };

              var marker = new google.maps.Marker({
                  position: myLatlng,
                  map: map,
                  title:"Hello World!",
                  icon: image
              });
            }
            google.maps.event.addDomListener(window, 'load', initialize);
          </script>
          <div id="map-canvas" class="article-map"></div>
          <div class="usercontent">
            <hr>
            <?php echo $page->text()->kirbytext() ?>
          </div>
        </div>
      </article>
    </div>
  </main>

<?php snippet('footer') ?>
