<?php if(!defined('KIRBY')) exit ?>

title: Site
pages: false
fields:
  meta:
    label: Metadaten
    type: headline
  title:
    label: Title
    type:  text
  url:
    label: URL
    type:  url
  author:
    label: Author
    type:  text
  description:
    label: Description
    type:  textarea
  address:
    label: Addresse
    type: headline
  latlng:
    label: Latitude, Longitude
    type: text
  street:
    label: Straße
    type:  text
  zip:
    label: Postleitzahl
    type:  text
    width: 1/4
  locality:
    label: Stadt
    type:  text
    width: 3/4
  region:
    label: Bundesland
    type:  text
    width: 1/2
  country:
    label: Land
    type:  text
    width: 1/2
  contact:
    label: Kontaktdaten
    type: headline
  email:
    label: E-Mail
    type:  email
  phone:
    label: Telefon
    type:  tel
  fax:
    label: Fax
    type:  tel
    icon:  fax
