<?php if(!defined('KIRBY')) exit ?>

title: Career
pages: true
  template: career-item
files: false
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea
