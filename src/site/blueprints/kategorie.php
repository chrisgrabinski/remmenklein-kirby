<?php if(!defined('KIRBY')) exit ?>

title: Kategorie
pages: true
  template: unterkategorie
files: false
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea
