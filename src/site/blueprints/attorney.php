<?php if(!defined('KIRBY')) exit ?>

title: Lawyer
pages: false
files: true
fields:
  title:
    label: Name
    type:  text
    icon: user
  field:
    label: Fachbereich
    type:  text
  text:
    label: Text
    type:  textarea
  contactphone:
    label: Telefon
    type:  tel
  contactmail:
    label: E-Mail
    type:  email
