<?php if(!defined('KIRBY')) exit ?>

title: Attorneys
pages:
  template: lawyer
files: false
fields:
  title:
    label: Title
    type:  text
  label:
    label: 'Zur Person'-Label
    type: text
