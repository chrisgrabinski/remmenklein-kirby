<?php if(!defined('KIRBY')) exit ?>

title: News Item
pages: false
files: false
fields:
  title:
    label: Title
    type:  text
  date:
    label: Datum
    type:  date
  text:
    label: Text
    type:  textarea
