<?php if(!defined('KIRBY')) exit ?>

title: Impressum
pages: false
files: false
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea
