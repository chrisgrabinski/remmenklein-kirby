<?php if(!defined('KIRBY')) exit ?>

title: News
pages:
  template: news-item
files: false
fields:
  title:
    label: Title
    type:  text
  headline:
    label: Überschrift
    type:  text
