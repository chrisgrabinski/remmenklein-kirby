<?php if(!defined('KIRBY')) exit ?>

title: Career Item
pages: false
files: false
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea
  address:
    label: Anschrift
    type:  textarea
  email:
    label: E-Mail
    type:  email
