<?php if(!defined('KIRBY')) exit ?>

title: Schwerpunkt
pages:
  template: kategorie
files: false
fields:
  title:
    label: Title
    type:  text
  text:
    label: Text
    type:  textarea
  subtext:
    label: Sekundärer Text
    type:  textarea
  phone:
    label: Telefonnummer
    type:  tel
  email:
    label: E-Mail-Adresse
    type:  email
  sidebar:
    label: Sidebar
    type: select
    default: none
    options:
      none: Keine
      news: News
      beratungsansatz: Beratungsansatz
  show_siblings:
    label: Schwerpunkte
    type: checkbox
    text: Auflistung der Schwerpunkte unterhalb des Contents anzeigen?
