<nav class="navigation" role="navigation">
  <ul class="navigation-list">
    <?php foreach($pages->find('schwerpunkte')->children() as $p): ?>
    <li class="navigation-item"><a class="navigation-link" href="<?php echo $p->url() ?>"><span><?php echo $p->title()->html() ?></span></a></li>
    <?php endforeach ?>
  </ul>
</nav>
