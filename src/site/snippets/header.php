<!DOCTYPE html>
<!--[if IE 8]>         <html class="is-ie8" lang="<?php echo $site->language->code(); ?>" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="<?php echo $site->language->code(); ?>" xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo str_replace('&shy;', '', $page->title()->html()) ?> &#124 <?php echo $site->title()->html() ?></title>
  <meta name="description" content="<?php if ( $page->isHomePage() ) { echo $site->description()->excerpt(); } else { echo $page->text()->excerpt(); } ?>">
  <meta property="og:title" content="<?php echo str_replace('&shy;', '', $page->title()->html()) ?>">
  <meta property="og:type" content="company">
  <meta property="og:url" content="<?php echo $site->url() ?>">
  <meta property="og:site_name" content="<?php echo $site->title()->html() ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="google-site-verification" content="...">
  <!--[if IE]><script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6/html5shiv.min.js"></script><![endif]-->
  <?php echo js('assets/scripts/modernizr.js') ?>
  <?php echo css('assets/css/vendor.css') ?>
  <?php echo css('assets/css/main.css') ?>
  <?php echo css('assets/css/print.css') ?>
  <!--[if lt IE 9]><?php echo css('assets/css/legacy.css') ?><![endif]-->
  <link rel="shortcut icon" href="<?= url('assets/images/favicon.ico') ?>">
  <link rel="author" href="humans.txt">
  <link rel="canonical" href="<?php echo $page->url() ?>">
  <script type="text/javascript">
    (function() {
        var path = '//easy.myfonts.net/v2/js?sid=210856(font-family=Avenir+Next+Pro+Demi)&sid=210860(font-family=Avenir+Next+Pro+Bold)&sid=217165(font-family=Avenir+Next+Pro)&sid=217166(font-family=Avenir+Next+Pro+Medium)&key=pDHgPMTNoq',
            protocol = ('https:' == document.location.protocol ? 'https:' : 'http:'),
            trial = document.createElement('script');
        trial.type = 'text/javascript';
        trial.async = true;
        trial.src = protocol + path;
        var head = document.getElementsByTagName("head")[0];
        head.appendChild(trial);
    })();
  </script>
</head>
<body class="<?php echo $page->uid(); ?><?php if(!$page->isHomePage()) :?> not-home<?php endif; ?>">
  <div class="page animsition">
    <header class="header-area" role="banner">
      <div class="container">
        <a class="header-area-logo" href="<?php echo $site->url() ?>" rel="home">
          <img src="<?php echo url('assets/images/logo.png') ?>" alt="Remmen + Klein">
        </a>
        <div class="header-area-navigation">
          <?php foreach($site->languages() as $language): ?>
          <a <?php e($site->language() == $language, 'class="header-area-button header-area-button-language language-is-active"', 'class="header-area-button header-area-button-language"') ?> href="<?php echo $page->url($language->code()) ?>">
            <abbr title="<?php echo html($language->name()) ?>"><?php echo html($language->code()) ?></abbr>
          </a>
          <?php endforeach; ?>
          <?php if(!$page->isHomePage()): ?>
            <a class="header-area-button header-area-button-menu" href="#">
              <span>Menu</span>
              <svg version="1.1" class="hamburger" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
                <rect class="close-02" x="15" y="1" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 16 38.6274)" fill="#0C78C7" width="2" height="30"></rect>
                <rect class="close-01" x="15" y="1" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -6.6274 16)" fill="#0C78C7" width="2" height="30"></rect>
                <rect class="open-top" x="1" y="4" fill="#0C78C7" width="30" height="2"></rect>
                <rect class="open-bottom" x="1" y="26" fill="#0C78C7" width="30" height="2"></rect>
              </svg>
            </a>
          <?php endif; ?>
        </div>
      </div>
    </header>
    <div class="navigation">
      <div class="container">
        <nav class="navigation-main">
          <ul class="navigation-items navigation-primary">
            <?php foreach( page('schwerpunkte')->children()->visible() as $field): ?>
            <li class="navigation-item">
              <a class="navigation-tile" href="<?php echo $field->url() ?>">
                <div class="center-container">
                  <span class="center-item"><?php echo $field->title()->html() ?></span>
                </div>
              </a>
            </li>
            <?php endforeach; ?>
          </ul>
          <?php if(!$page->isHomePage()): ?>
          <ul class="navigation-items navigation-secondary">
            <li class="navigation-item navigation-item-1">
              <div class="navigation-tile">
                <div class="center-container">
                  <div class="center-item">
                    <a href="<?php echo $pages->find('kontakt')->url() ?>"><?php echo $pages->find('kontakt')->title()->html() ?></a>
                  </div>
                </div>
              </div>
            </li>
            <li class="navigation-item navigation-item-2">
              <div class="navigation-tile">
                <div class="center-container">
                  <div class="center-item">
                    <?php if( $page->id() == 'schwerpunkte/arbeitsrecht' ): ?>
                      <a href="<?php echo $pages->find('anwaelte-arbeitsrecht')->url() ?>"><?php echo $pages->find('anwaelte')->title()->html() ?></a>
                    <?php else : ?>
                      <a href="<?php echo $pages->find('anwaelte')->url() ?>"><?php echo $pages->find('anwaelte')->title()->html() ?></a>
                    <?php endif ?>
                  </div>
                </div>
              </div>
            </li>
            <li class="navigation-item navigation-item-3">
              <div class="navigation-tile">
                <div class="center-container">
                  <div class="center-item">
                    <a href="<?php echo $pages->find('impressum')->url() ?>"><?php echo $pages->find('impressum')->title()->html() ?></a>
                  </div>
                </div>
              </div>
            </li>
            <li class="navigation-item navigation-item-4">
              <div class="navigation-tile">
                <div class="center-container">
                  <div class="center-item">
                    <a href="<?php echo $pages->find('karriere')->url() ?>"><?php echo $pages->find('karriere')->title()->html() ?></a>
                  </div>
                </div>
              </div>
            </li>
          </ul>
          <?php endif ?>
        </nav>
      </div>
    </div>
