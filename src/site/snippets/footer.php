    <footer class="footer-area" role="contentinfo">
      <div class="container">
        <ul class="footer-area-items">
          <li class="footer-area-item">
            <a class="footer-area-link" href="<?php echo $pages->find('kontakt')->url() ?>"><?php echo $pages->find('kontakt')->title()->html() ?></a>
          </li>
          <li class="footer-area-item">
            <?php if( $page->id() == 'schwerpunkte/arbeitsrecht' ): ?>
              <a class="footer-area-link" href="<?php echo $pages->find('anwaelte-arbeitsrecht')->url() ?>"><?php echo $pages->find('anwaelte')->title()->html() ?></a>
            <?php else : ?>
              <a class="footer-area-link" href="<?php echo $pages->find('anwaelte')->url() ?>"><?php echo $pages->find('anwaelte')->title()->html() ?></a>
            <?php endif ?>
          </li>
          <li class="footer-area-item">
            <a class="footer-area-link" href="<?php echo $pages->find('karriere')->url() ?>"><?php echo $pages->find('karriere')->title()->html() ?></a>
          </li>
          <li class="footer-area-item">
            <a class="footer-area-link" href="<?php echo $pages->find('impressum')->url() ?>" data-animsition-out="fade-in-up-sm" data-animsition-out-duration="1000"><?php echo $pages->find('impressum')->title()->html() ?></a>
          </li>
        </ul>
      </div>
    </footer>
  </div>
  <?php echo js('assets/scripts/vendor.js') ?>
  <?php echo js('assets/scripts/main.js') ?>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-XXXXXXXX-X', 'auto');ga('send', 'pageview');
  </script>
</body>
</html>
