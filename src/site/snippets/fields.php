<?php foreach(page('fachgebiete')->children()->visible() as $field): ?>
<section class="article-section accordion accordion-big">
  <header class="article-header">
    <i class="article-icon icon-contact"></i>
    <h1 class="article-title"><?php echo $field->title()->html() ?></h1>
  </header>
  <?php if ( $field->children()->visible()->count() >= 1 ) : ?>
  <dl class="accordion accordion-small">
  <?php foreach($field->children()->visible() as $subfield): ?>
    <dt class="accordion-title"><span><?php echo $subfield->title()->html() ?></span></dt>
    <dd class="accordion-content">
      <?php echo $subfield->text()->kirbytext() ?>
    </dd>
  <?php endforeach ?>
  </dl>
  <?php endif; ?>
</section>
<?php endforeach ?>
