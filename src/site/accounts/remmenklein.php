<?php if(!defined('KIRBY')) exit ?>

username: remmenklein
firstname: Remmen
lastname: Klein
email: mail@saschareeka.de
password: >
  $2a$10$sGDTWZiZgLszbdAvW59YgeRRgaG1TTi71RCOfmAdNIEC0CjsGkk8K
language: de
role: editor
token: debd03722c284ee4ddcaad3b1feb760e820f7aac
