<?php if(!defined('KIRBY')) exit ?>

username: chrisg
email: hello@chrisgrabinski.com
password: >
  $2a$10$0lej.pxzCF4T/q4z.9Hk2OlfHsQ.XGDEwmrNUQySynmWDTBxAqn5.
language: de
role: admin
history:
  - >
    news/neuer-gesetzesentwurf-fuer-kuerzere-kuendigungsfristen-veroeffentlicht
  - news/optimierte-webseite-online
  - >
    karriere/rechtsreferendarinnen-und-rechtsreferndaren
  - >
    karriere/rechtsanwaeltinnen-und-rechtsanwaelte
firstname:
lastname:
