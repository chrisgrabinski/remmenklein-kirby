'use strict';

var p               = require('./package.json'),
    gulp            = require('gulp'),
    plugin          = require('gulp-load-plugins')(),
    pngquant        = require('imagemin-pngquant'),
    mainBowerFiles  = require('main-bower-files'),
    browserSync     = require('browser-sync');

var src             = 'src/',
    dist            = 'dist/';

// Default Task
gulp.task('default', function() {
    gulp.start(['build']);
});

// Kirby Main Files

gulp.task('kirby-main-files', function() {
  gulp.src(src + 'assets/avatars/*.*')
 .pipe(gulp.dest(dist + 'assets/avatars/'));

  gulp.src(src + 'content/**/*.txt')
 .pipe(gulp.dest(dist + 'content/'));

  gulp.src(src + 'kirby/**/*.*')
 .pipe(gulp.dest(dist + 'kirby/'));

  gulp.src(src + 'panel/**/*.*')
 .pipe(gulp.dest(dist + 'panel/'));

  gulp.src(src + 'site/**/*.*')
 .pipe(gulp.dest(dist + 'site/'));

  gulp.src(src + 'thumbs/*.*')
 .pipe(gulp.dest(dist + 'thumbs/'));

  gulp.src(src + '.htaccess')
 .pipe(gulp.dest(dist));

  gulp.src(src + 'humans.txt')
 .pipe(gulp.dest(dist));

  gulp.src(src + 'index.php')
 .pipe(gulp.dest(dist));

  gulp.src(src + 'license.md')
 .pipe(gulp.dest(dist));

  gulp.src(src + 'readme.md')
 .pipe(gulp.dest(dist));
});

// Blueprints

gulp.task('blueprints', function() {
  gulp.src(src + 'site/blueprints/*.*')
 .pipe(gulp.dest(dist + 'site/blueprints/'))
 .pipe(browserSync.reload({stream: true}));
});

// Snippets

gulp.task('snippets', function() {
  gulp.src(src + 'site/snippets/*.*')
 .pipe(gulp.dest(dist + 'site/snippets/'))
 .pipe(browserSync.reload({stream: true}));
});

// Templates

gulp.task('templates', function() {
  gulp.src(src + 'site/templates/*.*')
 .pipe(gulp.dest(dist + 'site/templates/'))
 .pipe(browserSync.reload({stream: true}));
});

// Content

gulp.task('content', function() {
  gulp.src(src + 'site/content/**/*.*')
 .pipe(gulp.dest(dist + 'site/content/'))
 .pipe(browserSync.reload({stream: true}));
});


// Kirby Asset Files

gulp.task('assets', ['images', 'styles', 'scripts']);

// Images Task
gulp.task('images', function () {
    // Todo: Add imagemin
    gulp.src(src + 'assets/images/*')
    .pipe(plugin.imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
    .pipe(gulp.dest(dist + 'assets/images'))
    .pipe(plugin.size())
    .pipe(browserSync.reload({stream: true}));
});

// Styles Task
gulp.task('styles', function() {
  return gulp.src(src + 'assets/css/*.scss')
        .pipe(plugin.sourcemaps.init()) // Initiate sourcemaps
        .pipe(plugin.sass()) // Compile SASS
        .pipe(plugin.autoprefixer({ // Autoprefix CSS
            browsers: ['IE 8', 'IE 9', 'last 2 versions'],
            cascade: false
        }))
        .pipe(plugin.minifyCss())
        .pipe(plugin.sourcemaps.write('.')) // Write sourcemaps
        .pipe(gulp.dest(dist + 'assets/css'))
        .pipe(plugin.size())
        .pipe(plugin.filter('**/*.css'))
        .pipe(browserSync.reload({stream: true}));
});

// Scripts Task
gulp.task('scripts', function() {
  gulp.src(src + 'assets/scripts/*.js')
  .pipe(plugin.uglify())
  .pipe(gulp.dest(dist + 'assets/scripts'))
  .pipe(plugin.size())
  .pipe(browserSync.reload({stream: true}));
});



// Bower Task
// Concat main files into vendor.js
gulp.task('bower', function() {
   gulp.src(mainBowerFiles())
  .pipe(plugin.filter('*.js'))
  .pipe(plugin.concat('vendor.js'))
  .pipe(plugin.uglify())
  .pipe(gulp.dest(dist + '/assets/scripts'))
  .pipe(plugin.size());

   gulp.src(mainBowerFiles())
  .pipe(plugin.filter('*.css'))
  .pipe(plugin.concat('vendor.css'))
  .pipe(plugin.minifyCss())
  .pipe(gulp.dest(dist + '/assets/css'))
  .pipe(plugin.size());
});

// Connect Task
gulp.task('connect', function() {
  plugin.connectPhp.server({
    base: './dist'
  }, function (){
    browserSync({
      proxy: 'localhost:8000'
    });
  });

});


// Watch Task
gulp.task('watch', ['kirby-main-files', 'assets', 'bower', 'connect'], function() {
  gulp.watch([src + 'assets/css/**/*.{scss,css}'], ['styles']);
  gulp.watch([src + 'assets/scripts/**/*.js'], ['scripts']);
  gulp.watch([src + 'assets/images/**/*'], ['images']);
  gulp.watch([src + 'site/blueprints/**/*'], ['blueprints']);
  gulp.watch([src + 'site/templates/**/*'], ['templates']);
  gulp.watch([src + 'site/snippets/**/*'], ['snippets']);
  gulp.watch([src + 'content/**/*'], ['content']);
});

// Build Task
gulp.task('build', ['styles', 'scripts', 'html', 'images', 'extras', 'bower']);
